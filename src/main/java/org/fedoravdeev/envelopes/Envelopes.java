package org.fedoravdeev.envelopes;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Envelopes {

    public void run() {
        String answer;
        boolean result = false;
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("--\nProgram compare two envelops (a,b) and (c,d):");
            try {
                System.out.print("Input parameter first envelop\n'a':");
                float a = in.nextFloat();
                System.out.print("'b':");
                float b = in.nextFloat();
                System.out.print("Input parameter second envelop \n'c':");
                float c = in.nextFloat();
                System.out.print("'d':");
                float d = in.nextFloat();
                if (a * b > c * d && (!(max(c, d) > max(a, b))) && (!(min(c, d) <= min(a, b)))) {
                    result = true;
                }
                if (result) {
                    System.out.println("Envelop (c,d) fits in (a,b)");
                } else {
                    System.out.println("Envelop (c,d) not fits in (a,b)");
                }
            } catch (InputMismatchException ime) {
                System.out.println("Could you please input correct number.");
                in.nextLine();
            }

            System.out.print("--\nDo you want continue: y/n (yes/no)");
            answer = in.next();
            System.out.printf("Your answer: %s \n", answer);
        } while (answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("yes"));
        in.close();
    }
}
